/**
 * @file    proj3.c
 * @author  CYRIL URBAN
 * @date:   2015-12-07
 * @brief   Jednoducha shlukova analyza
 */

/**
 * Kostra programu pro 3. projekt IZP 2015/16
 *
 * Jednoducha shlukova analyza: 2D nejblizsi soused.
 * Single linkage
 * http://is.muni.cz/th/172767/fi_b/5739129/web/web/slsrov.html
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h> // sqrtf
#include <limits.h> // INT_MAX
#include <float.h> // FLT_MAX

/*****************************************************************
 * Ladici makra. Vypnout jejich efekt lze definici makra
 * NDEBUG, napr.:
 *   a) pri prekladu argumentem prekladaci -DNDEBUG
 *   b) v souboru (na radek pred #include <assert.h>
 *	  #define NDEBUG
 */
#ifdef NDEBUG
#define debug(s)
#define dfmt(s, ...)
#define dint(i)
#define dfloat(f)
#else

// vypise ladici retezec
#define debug(s) printf("- %s\n", s)

// vypise formatovany ladici vystup - pouziti podobne jako printf
#define dfmt(s, ...) printf(" - "__FILE__":%u: "s"\n",__LINE__,__VA_ARGS__)

// vypise ladici informaci o promenne - pouziti dint(identifikator_promenne)
#define dint(i) printf(" - " __FILE__ ":%u: " #i " = %d\n", __LINE__, i)

// vypise ladici informaci o promenne typu float - pouziti
// dfloat(identifikator_promenne)
#define dfloat(f) printf(" - " __FILE__ ":%u: " #f " = %g\n", __LINE__, f)

#endif

/*****************************************************************
 * Deklarace potrebnych datovych typu:
 *
 * TYTO DEKLARACE NEMENTE
 *
 *   struct obj_t - struktura objektu: identifikator a souradnice
 *   struct cluster_t - shluk objektu:
 *	  pocet objektu ve shluku,
 *	  kapacita shluku (pocet objektu, pro ktere je rezervovano
 *		  misto v poli),
 *	  ukazatel na pole shluku.
 */

struct obj_t
{
	int id;
	float x;
	float y;
};

struct cluster_t
{
	int size;
	int capacity;
	struct obj_t *obj;
};

/*****************************************************************
 * Deklarace potrebnych funkci.
 *
 * PROTOTYPY FUNKCI NEMENTE
 *
 * IMPLEMENTUJTE POUZE FUNKCE NA MISTECH OZNACENYCH 'TODO'
 *
 */

/*
 Inicializace shluku 'c'. Alokuje pamet pro cap objektu (kapacitu).
 Ukazatel NULL u pole objektu znamena kapacitu 0.
*/
void init_cluster(struct cluster_t *c, int cap)
{
	assert(c != NULL);
	assert(cap >= 0);

	if (c != NULL) // konotrola platnosti pointeru
	{
		c->size = 0; // inicializacni cast
		c->capacity = cap;
		c->obj = NULL;

		if (cap > 0)
		{
			c->obj = malloc(sizeof(struct obj_t) * cap); // alokace mista pro cap objektu = shluk
			if (c == NULL)
			{
				fprintf(stderr, "memory allocation failed\n");
				exit(0);
			}
		}
	}
}

/*
 Odstraneni vsech objektu shluku a inicializace na prazdny shluk.
 */
void clear_cluster(struct cluster_t *c)
{
	if ( c != NULL)
	{
		if (c->capacity)
		{
			free(c->obj);
		}
		init_cluster(c, 0);
	}
}

/// Chunk of cluster objects. Value recommended for reallocation.
const int CLUSTER_CHUNK = 10;

/*
 Zmena kapacity shluku 'c' na kapacitu 'new_cap'.
 */
struct cluster_t *resize_cluster(struct cluster_t *c, int new_cap)
{
	// TUTO FUNKCI NEMENTE
	assert(c);
	assert(c->capacity >= 0);
	assert(new_cap >= 0);

	if (c->capacity >= new_cap)
		return c;

	size_t size = sizeof(struct obj_t) * new_cap;

	void *arr = realloc(c->obj, size);
	if (arr == NULL)
		return NULL;

	c->obj = arr;
	c->capacity = new_cap;
	return c;
}

/*
 Prida objekt 'obj' na konec shluku 'c'. Rozsiri shluk, pokud se do nej objekt
 nevejde.
 */
void append_cluster(struct cluster_t *c, struct obj_t obj)
{
	if (c != NULL)
	{
		if (c->size >= c->capacity)
		{
			resize_cluster(c, c->capacity + CLUSTER_CHUNK);
		}

		c->obj[c->size] = obj;
		c->size++;
	}
}

/*
 Seradi objekty ve shluku 'c' vzestupne podle jejich identifikacniho cisla.
 */
void sort_cluster(struct cluster_t *c);


/*
 Do shluku 'c1' prida objekty 'c2'. Shluk 'c1' bude v pripade nutnosti rozsiren.
 Objekty ve shluku 'c1' budou serazny vzestupne podle identifikacniho cisla.
 Shluk 'c2' bude nezmenen.
 */
void merge_clusters(struct cluster_t *c1, struct cluster_t *c2)
{
	assert(c1 != NULL);
	assert(c2 != NULL);

	if (c1 != NULL && c2 != NULL)
	{
		for (int velikost = c2->size - 1; velikost >= 0; velikost--)
		{
			append_cluster(c1, c2->obj[velikost]);

		}
		sort_cluster(c1);
	}

}

/**********************************************************************/
/* Prace s polem shluku */

/*
 Odstrani shluk z pole shluku 'carr'. Pole shluku obsahuje 'narr' polozek
 (shluku). Shluk pro odstraneni se nachazi na indexu 'idx'. Funkce vraci novy
 pocet shluku v poli.
*/
int remove_cluster(struct cluster_t *carr, int narr, int idx)
{
	assert(idx < narr);
	assert(narr > 0);

	struct cluster_t pom;
	for (; idx < narr - 1; idx++)
	{
		pom = carr[idx + 1];
		carr[idx + 1] = carr[idx];
		carr[idx] = pom;
	}

	clear_cluster(&carr[narr - 1]);
	return narr - 1;
}

/*
 Pocita Euklidovskou vzdalenost mezi dvema objekty.
 */
float obj_distance(struct obj_t *o1, struct obj_t *o2)
{
	assert(o1 != NULL);
	assert(o2 != NULL);

	if (o1 != NULL && o2 != NULL)
	{
		float x = o1->x - o2->x;
		float y = o1->y - o2->y;
		float delka = sqrtf((x * x) + (y * y));
		return delka; // odmocnina z souctu mocnin x a y
	}
	return -1;
}

/*
 Pocita vzdalenost dvou shluku. Vzdalenost je vypoctena na zaklade nejblizsiho
 souseda.
*/
float cluster_distance(struct cluster_t *c1, struct cluster_t *c2)
{
	assert(c1 != NULL);
	assert(c1->size > 0);
	assert(c2 != NULL);
	assert(c2->size > 0);

	if (c1 != NULL && c2 != NULL)
	{
		struct obj_t *objekt1 = c1->obj;
		struct obj_t *objekt2 = c2->obj;

		float min = FLT_MAX; // nejvetsi mozna vzdalenost
		float delka;

		// meri vzdalenosti mezi vsemi objekty ze zhluku c1 a objekty c2
		for (int ic1 = 0; ic1 < c1->size; ic1++)
		{
			for (int ic2 = 0; ic2 < c2->size; ic2++)
			{
				delka = obj_distance(&objekt1[ic1], &objekt2[ic2]);
				if (delka < min)
				{
					min = delka;
				}
			}
		}
		return min;
	}
	return -1;
}

/*
 Funkce najde dva nejblizsi shluky. V poli shluku 'carr' o velikosti 'narr'
 hleda dva nejblizsi shluky (podle nejblizsiho souseda). Nalezene shluky
 identifikuje jejich indexy v poli 'carr'. Funkce nalezene shluky (indexy do
 pole 'carr') uklada do pameti na adresu 'c1' resp. 'c2'.
*/
void find_neighbours(struct cluster_t *carr, int narr, int *c1, int *c2)
{
	assert(narr > 0);

	if (carr != NULL && c1 != NULL && c2 != NULL)
	{
		float min = FLT_MAX; // nejvetsi mozna vzdalenost
		float delka;

		for (int i = 0; i < narr; i++)
		{
			for (int j = i + 1; j < narr; j++)
			{
				delka = cluster_distance(&carr[i], &carr[j]); // meri delku dvou zhluku

				if (delka < min)
				{
					min = delka;
					*c1 = i;
					*c2 = j;
				}

			}
		}
	}
}

// pomocna funkce pro razeni shluku
static int obj_sort_compar(const void *a, const void *b)
{
	// TUTO FUNKCI NEMENTE
	const struct obj_t *o1 = a;
	const struct obj_t *o2 = b;
	if (o1->id < o2->id) return -1;
	if (o1->id > o2->id) return 1;
	return 0;
}

/*
 Razeni objektu ve shluku vzestupne podle jejich identifikatoru.
*/
void sort_cluster(struct cluster_t *c)
{
	// TUTO FUNKCI NEMENTE
	qsort(c->obj, c->size, sizeof(struct obj_t), &obj_sort_compar);
}

/*
 Tisk shluku 'c' na stdout.
*/
void print_cluster(struct cluster_t *c)
{
	// TUTO FUNKCI NEMENTE
	for (int i = 0; i < c->size; i++)
	{
		if (i) putchar(' ');
		printf("%d[%g,%g]", c->obj[i].id, c->obj[i].x, c->obj[i].y);
	}
	putchar('\n');
}

/*
 Ze souboru 'filename' nacte objekty. Pro kazdy objekt vytvori shluk a ulozi
 jej do pole shluku. Alokuje prostor pro pole vsech shluku a ukazatel na prvni
 polozku pole (ukalazatel na prvni shluk v alokovanem poli) ulozi do pameti,
 kam se odkazuje parametr 'arr'. Funkce vraci pocet nactenych objektu (shluku).
 V pripade nejake chyby uklada do pameti, kam se odkazuje 'arr', hodnotu NULL.
*/
int load_clusters(char *filename, struct cluster_t **arr)
{
	assert(arr != NULL);

	if (filename != NULL)
	{
		FILE *soubor = NULL;
		int pocet = 0;
		struct obj_t objekt;
		*arr = NULL;

		if ((soubor = fopen(filename, "r")) == NULL) // overi platnost souboru
		{
			fprintf(stderr, "invalid open file for reading\n");
			return -1;
		}

		fscanf(soubor, "count=%d\n", &pocet);

		if (pocet <= 0)
		{
			fprintf(stderr, "invalid number of objects\n");
			fclose(soubor);
			return -1;
		}

		*arr = malloc(sizeof(struct cluster_t) * pocet); // alokace pameti pro pole shluku

		if ((*arr) == NULL)
		{
			fprintf(stderr, "memory allocation failed\n");
			fclose(soubor);
			return -1;
		}

		for (int i = 0; i < pocet; i++)
		{
			init_cluster(&(*arr)[i], CLUSTER_CHUNK); // vytvarim shluk na indexu 'i' v poli zhluku o doporucene kapacite
			fscanf(soubor, "%d %f %f", &objekt.id, &objekt.x, &objekt.y); // ze souboru vytvarim objekt
			append_cluster(&(*arr)[i], objekt); // pridam objekt do tohoto zhluku
		}

		fclose(soubor);
		return pocet; // vrati pocet shluku
	}
	return -1;
}

/*
 Tisk pole shluku. Parametr 'carr' je ukazatel na prvni polozku (shluk).
 Tiskne se prvnich 'narr' shluku.
*/
void print_clusters(struct cluster_t *carr, int narr)
{
	printf("Clusters:\n");
	for (int i = 0; i < narr; i++)
	{
		printf("cluster %d: ", i);
		print_cluster(&carr[i]);
	}
}

int main(int argc, char *argv[])
{
	struct cluster_t *clusters;

	if (argc == 3)
	{
		char *soubor;
		soubor = argv[1];
		char *text;
		text = '\0';

		int shluky = strtol(argv[2], &text, 10);


		// nacte mnozstvi shluku, alokoje pamet, vytvori pole
		int mnozstvi = load_clusters(soubor, &clusters);

		int i , j;

		while (mnozstvi > shluky)
		{
			find_neighbours(clusters, mnozstvi, &i, &j);
			merge_clusters(&clusters[i], &clusters[j]);
			mnozstvi = remove_cluster(clusters, mnozstvi, j);
		}

		// tisk shluku
		print_clusters(&clusters[0], shluky);

		// uvolneni pameti
		while (mnozstvi >= 0)
		{
			clear_cluster(&clusters[mnozstvi]);
			mnozstvi--;
		}

		free(&clusters[0]);

	}

	return 0;
}